function substituir(str) {
    return str.replace(/a/gi, 'x'); //replace usado para modificar a string. /a/ é o string, g é de globar, i é de ignorar, x é o que vira no
    //lugar do que foi trocado
}

function converter(str) {
return str.toUpperCase();
}
//touppercase é usado para trocar letras por maisculas

function exibir(str){
   console.log(`Resultado final : ${str} `); //cifrao é uma concatenação
}

const data =  { 
"texto" : "Senac hoje de tarde" ,
"aporx" : "" ,
"upper" : ""
} //data é como se fosse uma variavel-array que guarda as coisas para poder acessar

data.aporx = substituir(data.texto);
data.upper = converter(data.aporx);
exibir(data.upper);