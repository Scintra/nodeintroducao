const usuarios =
[
    {
        "nome": "Stela",
        "idade": 20,
        "cidade": "Pindamonhangaba",
        "email": "stela_cintra@yahoo.com.br",
        "documentos": {
            "rg": "500202158",
            "cpf": "39358847720"
        },
        "carros": [
            "fiesta ",
            "sandero"
        ]
    },
    {
        "nome": "José",
        "idade": 30,
        "cidade": "Piracuama",
        "email": "jose_silvaa@yahoo.com.br",
        "documentos": {
            "rg": "530203152",
            "cpf": "39558742721"
        },
        "carros": [
            "brasilia ",
            " logan"
        ]
    },
    {
        "nome": "Pedro",
        "idade": 25,
        "cidade": "Roseira",
        "email": "paulo@yahoo.com.br",
        "documentos": {
            "rg": "670202853",
            "cpf": "2935884178"
        },
        "carros": [
            "fusca "
        ]
    }
]
//for (i =0 ; i<=2; i++){ para percorrer os usuarios e mostrar os dados
    usuarios.forEach(function (usuario) { //foreach para abrir um array dentro de um objeto 

    console.log( `Nome" ${usuario.nome}`);
    console.log(` RG ${usuario.documentos.rg}`);
    console.log(`Idade ${usuario.idade}`);

  usuario.carros.forEach(function (carro) {
      console.log(carro)
  });
    console.log("--------")
});


//const usuario = usuarios[1].nome; 
//const cidade = usuarios[1].cidade
//const rg = usuarios[1].documentos.rg;
//const cpf = usuarios[1].documentos.cpf
//const carros = usuarios[1].carros[1];
 //console.log(usuario);
 //console.log(cidade);
 //console.log(rg);
 //console.log(cpf)
 //console.log(carros)



 
